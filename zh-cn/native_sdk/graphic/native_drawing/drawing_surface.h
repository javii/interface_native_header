/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_GPU_SURFACE_H
#define C_INCLUDE_DRAWING_GPU_SURFACE_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。
 * 本模块不提供像素单位，和应用上下文环境保持一致。如果处于ArkUI开发环境中，采用框架默认像素单位vp。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_surface.h
 *
 * @brief 文件中定义与surface相关的功能函数，包括surface的创建、销毁和使用等。
 *
 * 引用文件"native_drawing/drawing_surface.h"
 * @library libnative_drawing.so
 * @since 12
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 使用图形处理器上下文创建一个surface对象，用于管理画布绘制的内容。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_GpuContext 指向图形处理器上下文对象的指针{@link OH_Drawing_GpuContext}。
 * @param budgeted 用于控制内存分配是否计入缓存预算，true则计入高速缓存预算，false则不计入高速缓存预算。
 * @param OH_Drawing_Image_Info 指向图片信息{@link OH_Drawing_Image_Info}的指针。
 * @return 返回一个指针，指针指向创建的surface对象{@link OH_Drawing_Surface}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Surface* OH_Drawing_SurfaceCreateFromGpuContext(
    OH_Drawing_GpuContext*, bool budgeted, OH_Drawing_Image_Info);

/**
 * @brief 通过surface对象获取画布对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Surface 指向创建的surface对象的指针。
 * @return 返回一个指针，指针指向创建的画布对象{@link OH_Drawing_Canvas}。返回的指针不需要由调用者管理。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Canvas* OH_Drawing_SurfaceGetCanvas(OH_Drawing_Surface*);

/**
 * @brief 销毁surface对象并回收该对象占用的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Surface 指向创建的surface对象的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SurfaceDestroy(OH_Drawing_Surface*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
