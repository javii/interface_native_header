/*
 * Copyright (c) 2023-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_MATRIX_H
#define C_INCLUDE_DRAWING_MATRIX_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。
 * 本模块不提供像素单位，和应用上下文环境保持一致。如果处于ArkUI开发环境中，采用框架默认像素单位vp。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_matrix.h
 *
 * @brief 文件中定义了与矩阵相关的功能函数。
 *
 * 引用文件"native_drawing/drawing_matrix.h"
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 用于创建一个矩阵对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 函数会返回一个指针，指针指向创建的矩阵对象。
 * @since 11
 * @version 1.0
 */
OH_Drawing_Matrix* OH_Drawing_MatrixCreate(void);

/**
 * @brief 创建一个带旋转属性的矩阵对象。
 * 该矩阵对象为：单位矩阵在(x, y)旋转点以度为单位进行旋转后得到的矩阵。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param deg  旋转的角度，单位为度。正数表示按顺时针旋转，负数表示按逆时针旋转。
 * @param x  x轴上坐标点。
 * @param y  y轴上坐标点。
 * @return 函数返回一个指针，指针指向创建的矩阵对象{@link OH_Drawing_Matrix}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Matrix* OH_Drawing_MatrixCreateRotation(float deg, float x, float y);

/**
 * @brief 创建一个带缩放属性的矩阵对象。
 * 该矩阵对象为：单位矩阵在(px, py)旋转点以sx和sy为缩放因子进行缩放后得到的矩阵。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param sx  水平缩放因子。
 * @param sy  垂直缩放因子。
 * @param px  x轴上坐标点。
 * @param py  y轴上坐标点。
 * @return 函数返回一个指针，指针指向创建的矩阵对象{@link OH_Drawing_Matrix}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Matrix* OH_Drawing_MatrixCreateScale(float sx, float sy, float px, float py);

/**
 * @brief 创建一个带平移属性的矩阵对象。
 * 该矩阵对象为：单位矩阵平移(dx, dy)后得到的矩阵。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param dx  水平方向平移距离。
 * @param dy  垂直方向平移距离。
 * @return 函数返回一个指针，指针指向创建的矩阵对象{@link OH_Drawing_Matrix}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Matrix* OH_Drawing_MatrixCreateTranslation(float dx, float dy);

/**
 * @brief 用于给矩阵对象设置参数。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象的指针。
 * @param scaleX  水平缩放系数。
 * @param skewX   水平倾斜系数。
 * @param transX  水平位移系数。
 * @param skewY   垂直倾斜系数。
 * @param scaleY  垂直缩放系数。
 * @param transY  垂直位移系数。
 * @param persp0  X轴透视系数。
 * @param persp1  Y轴透视系数。
 * @param persp2  透视缩放系数。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_MatrixSetMatrix(OH_Drawing_Matrix*, float scaleX, float skewX, float transX,
    float skewY, float scaleY, float transY, float persp0, float persp1, float persp2);

/**
 * @brief 矩阵缩放方式枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_ScaleToFit {
    /**
     * 按水平轴和垂直轴缩放以填充目标矩形。
     */
    SCALE_TO_FIT_FILL,
    /**
     * 缩放并对齐到左侧和顶部。
     */
    SCALE_TO_FIT_START,
    /**
     * 缩放并居中对齐。
     */
    SCALE_TO_FIT_CENTER,
    /**
     * 缩放并向右和向下对齐。
     */
    SCALE_TO_FIT_END,
} OH_Drawing_ScaleToFit;

/**
 * @brief 将矩阵以缩放方式适配目标矩阵。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param src 指向映射源的{@link OH_Drawing_Rect}对象Rect的指针。
 * @param dst 指向要映射到的{@link OH_Drawing_Rect}对象Rect的指针。
 * @param stf 缩放方式，支持方式{@link OH_Drawing_ScaleToFit}。
 * @return 如果设置失败，则返回false；如果设置成功，则返回true；如果矩阵为空，则返回true，并将矩阵设置为：
 *         | 0 0 0 |
 *         | 0 0 0 |
 *         | 0 0 1 |
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_MatrixSetRectToRect(OH_Drawing_Matrix*, const OH_Drawing_Rect* src,
    const OH_Drawing_Rect* dst, OH_Drawing_ScaleToFit stf);

/**
 * @brief 将矩阵设置为矩阵左乘围绕轴心点旋转一定角度的单位矩阵后得到的矩阵。
 *        例如给定的矩阵：
 *
 *                     | A B C |                        | c -s dx |
 *            Matrix = | D E F |,  R(degrees, px, py) = | s  c dy |
 *                     | G H I |                        | 0  0  1 |
 *
 *        条件为：
 *
 *            c  = cos(degrees)
 *            s  = sin(degrees)
 *            dx =  s * py + (1 - c) * px
 *            dy = -s * px + (1 - c) * py
 *
 *        设置的最终矩阵为：
 *
 *                                          | A B C | | c -s dx |   | Ac+Bs -As+Bc A*dx+B*dy+C |
 *            Matrix * R(degrees, px, py) = | D E F | | s  c dy | = | Dc+Es -Ds+Ec D*dx+E*dy+F |
 *                                          | G H I | | 0  0  1 |   | Gc+Hs -Gs+Hc G*dx+H*dy+I |
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param degree 旋转角度，单位为度。正数表示顺时针旋转，负数表示逆时针旋转。
 * @param px 旋转中心点的横坐标。
 * @param py 旋转中心点的纵坐标。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_MatrixPreRotate(OH_Drawing_Matrix*, float degree, float px, float py);

/**
 * @brief 将矩阵设置为矩阵左乘围绕轴心点按一定缩放因子缩放后的单位矩阵后得到的矩阵。
 *        例如给定的矩阵：
 *
 *                    | A B C |                       | sx  0 dx |
 *            Matrix =| D E F |,  S(sx, sy, px, py) = |  0 sy dy |
 *                    | G H I |                       |  0  0  1 |
 *
 *        条件为：
 *
 *            dx = px - sx * px
 *            dy = py - sy * py
 *
 *        设置的最终矩阵为：
 *
 *                                         | A B C | | sx  0 dx |   | A*sx B*sy A*dx+B*dy+C |
 *            Matrix * S(sx, sy, px, py) = | D E F | |  0 sy dy | = | D*sx E*sy D*dx+E*dy+F |
 *                                         | G H I | |  0  0  1 |   | G*sx H*sy G*dx+H*dy+I |
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param sx x轴方向的缩放比例因子。
 * @param sy y轴方向的缩放比例因子。
 * @param px 缩放中心点的横坐标。
 * @param py 缩放中心点的纵坐标。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_MatrixPreScale(OH_Drawing_Matrix*, float sx, float sy, float px, float py);

/**
 * @brief 将矩阵设置为矩阵左乘平移一定距离后的单位矩阵后得到的矩阵。
 *        例如给定的矩阵：
 *
 *                     | A B C |               | 1 0 dx |
 *            Matrix = | D E F |,  T(dx, dy) = | 0 1 dy |
 *                     | G H I |               | 0 0  1 |
 *
 *        设置的最终矩阵为：
 *
 *                                 | A B C | | 1 0 dx |   | A B A*dx+B*dy+C |
 *            Matrix * T(dx, dy) = | D E F | | 0 1 dy | = | D E D*dx+E*dy+F |
 *                                 | G H I | | 0 0  1 |   | G H G*dx+H*dy+I |
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param dx 表示在x轴方向上的平移距离。
 * @param dy 表示在y轴方向上的平移距离。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_MatrixPreTranslate(OH_Drawing_Matrix*, float dx, float dy);

/**
 * @brief 将矩阵设置为矩阵右乘围绕轴心点旋转一定角度的单位矩阵后得到的矩阵。
 *        例如给定的矩阵：
 *
 *                     | J K L |                        | c -s dx |
 *            Matrix = | M N O |,  R(degrees, px, py) = | s  c dy |
 *                     | P Q R |                        | 0  0  1 |
 *
 *        条件为：
 *
 *            c  = cos(degrees)
 *            s  = sin(degrees)
 *            dx =  s * py + (1 - c) * px
 *            dy = -s * px + (1 - c) * py
 *
 *        设置的最终的矩阵为：
 *
 *                                          |c -s dx| |J K L|   |cJ-sM+dx*P cK-sN+dx*Q cL-sO+dx+R|
 *            R(degrees, px, py) * Matrix = |s  c dy| |M N O| = |sJ+cM+dy*P sK+cN+dy*Q sL+cO+dy*R|
 *                                          |0  0  1| |P Q R|   |         P          Q          R|
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param degree 旋转角度，单位为度。正数表示顺时针旋转，负数表示逆时针旋转。
 * @param px 旋转中心点的横坐标。
 * @param py 旋转中心点的纵坐标。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_MatrixPostRotate(OH_Drawing_Matrix*, float degree, float px, float py);

/**
 * @brief 将矩阵设置为矩阵右乘围绕轴心点按一定缩放因子缩放后的单位矩阵后得到的矩阵。
 *        例如给定的矩阵：
 *
 *                     | J K L |                       | sx  0 dx |
 *            Matrix = | M N O |,  S(sx, sy, px, py) = |  0 sy dy |
 *                     | P Q R |                       |  0  0  1 |
 *
 *        条件为：
 *            dx = px - sx * px
 *            dy = py - sy * py
 *
 *        设置的最终的矩阵为：
 *
 *                                         | sx  0 dx | | J K L |   | sx*J+dx*P sx*K+dx*Q sx*L+dx+R |
 *            S(sx, sy, px, py) * Matrix = |  0 sy dy | | M N O | = | sy*M+dy*P sy*N+dy*Q sy*O+dy*R |
 *                                         |  0  0  1 | | P Q R |   |         P         Q         R |
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param sx x轴方向的缩放比例因子。
 * @param sy y轴方向的缩放比例因子。
 * @param px 缩放中心点的横坐标。
 * @param py 缩放中心点的纵坐标。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_MatrixPostScale(OH_Drawing_Matrix*, float sx, float sy, float px, float py);

/**
 * @brief 将矩阵设置为矩阵右乘平移一定距离后的单位矩阵后得到的矩阵。
 *        例如给定的矩阵：
 *
 *                     | J K L |               | 1 0 dx |
 *            Matrix = | M N O |,  T(dx, dy) = | 0 1 dy |
 *                     | P Q R |               | 0 0  1 |
 *
 *        设置的最终的矩阵为：
 *
 *                                 | 1 0 dx | | J K L |   | J+dx*P K+dx*Q L+dx*R |
 *            T(dx, dy) * Matrix = | 0 1 dy | | M N O | = | M+dy*P N+dy*Q O+dy*R |
 *                                 | 0 0  1 | | P Q R |   |      P      Q      R |
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param dx 表示在x轴方向上的平移距离。
 * @param dy 表示在y轴方向上的平移距离。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_MatrixPostTranslate(OH_Drawing_Matrix*, float dx, float dy);

/**
 * @brief 重置当前矩阵为单位矩阵:
 *        | 1 0 0 |
 *        | 0 1 0 |
 *        | 0 0 1 |。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_MatrixReset(OH_Drawing_Matrix*);

/**
 * @brief 将矩阵total设置为矩阵a乘以矩阵b。
 *       例如给定矩阵a和矩阵b如下所示:
 *                    | A B C |          | J K L |
 *                a = | D E F |,     b = | M N O |
 *                    | G H I |          | P Q R |
 *       设置的最终矩阵total为:
 *                            | A B C |   | J K L |   | AJ+BM+CP AK+BN+CQ AL+BO+CR |
 *           total = a * b =  | D E F | * | M N O | = | DJ+EM+FP DK+EN+FQ DL+EO+FR |
 *                            | G H I |   | P Q R |   | GJ+HM+IP GK+HN+IQ GL+HO+IR |
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param total 指向最终的矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param a 指向矩阵对象a{@link OH_Drawing_Matrix}的指针。
 * @param b 指向矩阵对象b{@link OH_Drawing_Matrix}的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_MatrixConcat(OH_Drawing_Matrix* total, const OH_Drawing_Matrix* a,
    const OH_Drawing_Matrix* b);

/**
 * @brief 获取矩阵给定索引位的值。索引范围0-8。
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param index 索引位置，范围0-8。
 * @return 函数返回矩阵给定索引位对应的值。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_MatrixGetValue(OH_Drawing_Matrix*, int index);

/**
 * @brief 设置矩阵为单位矩阵，并围绕位于(px, py)的旋转轴点进行旋转。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param degree 角度，单位为度。正数表示顺时针旋转，负数表示逆时针旋转。
 * @param px x轴上坐标点。
 * @param py y轴上坐标点。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_MatrixRotate(OH_Drawing_Matrix*, float degree, float px, float py);

/**
 * @brief 设置矩阵为单位矩阵，并平移(dx, dy)。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param dx 水平方向平移距离。
 * @param dy 垂直方向平移距离。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_MatrixTranslate(OH_Drawing_Matrix*, float dx, float dy);

/**
 * @brief 设置矩阵为单位矩阵，并围绕位于(px, py)的旋转轴点，以sx和sy进行缩放。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param sx 水平缩放因子。
 * @param sy 垂直缩放因子。
 * @param px x轴上坐标点。
 * @param py y轴上坐标点。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_MatrixScale(OH_Drawing_Matrix*, float sx, float sy, float px, float py);

/**
 * @brief 将矩阵inverse设置为矩阵的倒数，并返回结果。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param inverse 指向逆矩阵对象{@link OH_Drawing_Matrix}的指针，
 * 开发者可调用{@link OH_Drawing_MatrixCreate}接口创建。
 * @return 函数返回true表示矩阵可逆，inverse被填充为逆矩阵；函数返回false表示矩阵不可逆，inverse不被改变。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_MatrixInvert(OH_Drawing_Matrix*, OH_Drawing_Matrix* inverse);

/**
 * @brief 通过设置源点以及目标点，生成对应的变换矩阵。
 * 源点以及目标点的个数要大于等于0，小于等于4。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param src 源点数组。
 * @param dst 目标点数组，个数要与源点相等。
 * @param count 源点数组以及目标点数组的个数。
 * @return 函数返回是否可以生成对应矩阵以用来完成变换。true表示矩阵生成成功，false表示无法生成对应矩阵。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_MatrixSetPolyToPoly(OH_Drawing_Matrix*, const OH_Drawing_Point2D* src,
    const OH_Drawing_Point2D* dst, uint32_t count);

/**
 * @brief 判断两个矩阵是否相等。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向用于判断的其中一个矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @param other 指向用于判断的另一个矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @return 函数返回两个矩阵的比较结果，返回true表示两个矩阵相等，返回false表示两个矩阵不相等。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_MatrixIsEqual(OH_Drawing_Matrix*, OH_Drawing_Matrix* other);

/**
 * @brief 判断矩阵是否是单位矩阵。
 * 单位矩阵为 :  | 1 0 0 |
 *              | 0 1 0 |
 *              | 0 0 1 |
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向矩阵对象{@link OH_Drawing_Matrix}的指针。
 * @return 函数返回true表示矩阵是单位矩阵，函数返回false表示矩阵不是单位矩阵。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_MatrixIsIdentity(OH_Drawing_Matrix*);

/**
 * @brief 用于销毁矩阵对象并回收该对象占有的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix 指向字体对象的指针。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_MatrixDestroy(OH_Drawing_Matrix*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
