/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OH_CURSOR_H
#define OH_CURSOR_H

/**
 * @addtogroup RDB
 * @{
 *
 * @brief 关系型数据库（Relational Database，RDB）是一种基于关系模型来管理数据的数据库。关系型数据库基于SQLite组件提供了一套完整的
 * 对本地数据库进行管理的机制，对外提供了一系列的增、删、改、查等接口，也可以直接运行用户输入的SQL语句来满足复杂的场景需要。
 *
 * @syscap SystemCapability.DistributedDataManager.RelationalStore.Core
 * @since 10
 */

/**
 * @file oh_cursor.h
 *
 * @brief 提供通过查询数据库生成的数据库结果集的访问方法。
 *
 * 结果集是指用户调用关系型数据库查询接口之后返回的结果集合，提供了多种灵活的数据访问方式，以便用户获取各项数据。
 * 引用文件: <database/rdb/oh_cursor.h>
 * @library libnative_rdb_ndk.z.so
 * @since 10
 */

#include <cstdint>
#include <stddef.h>
#include <stdbool.h>
#include "database/rdb/data_asset.h"
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 数据库字段类型.
 *
 * @since 10
 */
typedef enum OH_ColumnType {
    /** 表示NULL类型 */
    TYPE_NULL = 0,
    /** 表示INT64数据类型 */
    TYPE_INT64,
    /** 表示REAL数据类型 */
    TYPE_REAL,
    /** 表示TEXT数据类型 */
    TYPE_TEXT,
    /** 表示BLOB数据类型 */
    TYPE_BLOB,
    /**
     * @brief 表示ASSET（资产附件）数据类型
     *
     * @since 11
     */
    TYPE_ASSET,
    /**
     * @brief ASSETS（多个资产附件）数据类型
     *
     * @since 11
     */
    TYPE_ASSETS,
} OH_ColumnType;

/**
 * @brief 表示结果集。
 *
 * 提供通过查询数据库生成的数据库结果集的访问方法。
 *
 * @since 10
 */
typedef struct OH_Cursor {
    /** @brief OH_Cursor结构体的唯一标识符。 */
    int64_t id;

    /**
     * @brief 函数指针，获取结果集中的列数。
     *
     * @param cursor 表示指向{@link OH_Cursor}实例的指针。
     * @param count 该参数是输出参数，结果集中的列数会写入该变量。
     * @return 返回操作是否成功，出错时返回对应的错误码。
     * @see OH_Cursor.
     * @since 10
     */
    int (*getColumnCount)(OH_Cursor *cursor, int *count);

    /**
     * @brief 函数指针，根据指定的列索引获取列类型。
     *
     * @param cursor 表示指向{@link OH_Cursor}实例的指针。
     * @param columnIndex 表示结果集中指定列的索引, 索引值从0开始。
     * @param columnType 该参数是输出参数，结果集中指定列的数据类型{@link OH_ColumnType}会写入该变量。
     * @return 返回操作是否成功，出错时返回对应的错误码。
     * @see OH_Cursor, OH_ColumnType.
     * @since 10
     */
    int (*getColumnType)(OH_Cursor *cursor, int32_t columnIndex, OH_ColumnType *columnType);

    /**
     * @brief 函数指针，根据指定的列名获取列索引。
     *
     * @param cursor 表示指向{@link OH_Cursor}实例的指针。
     * @param name 表示结果集中指定列的名称。
     * @param columnIndex 该参数是输出参数，结果集中指定列的索引会写入该变量。
     * @return 返回操作是否成功，出错时返回对应的错误码。
     * @see OH_Cursor.
     * @since 10
     */
    int (*getColumnIndex)(OH_Cursor *cursor, const char *name, int *columnIndex);

    /**
     * @brief 函数指针，根据指定的列索引获取列名。
     *
     * @param cursor 表示指向{@link OH_Cursor}实例的指针。
     * @param columnIndex 表示结果集中指定列的索引, 索引值从0开始。
     * @param name 该参数是输出参数，结果集中指定列的名称会写入该变量。
     * @param length 表示列名的长度。
     * @return 返回操作是否成功，出错时返回对应的错误码。
     * @see OH_Cursor.
     * @since 10
     */
    int (*getColumnName)(OH_Cursor *cursor, int32_t columnIndex, char *name, int length);

    /**
     * @brief 函数指针，获取结果集中的行数。
     *
     * @param cursor 表示指向{@link OH_Cursor}实例的指针。
     * @param count 该参数是输出参数，结果集中的行数会写入该变量。
     * @return 返回操作是否成功，出错时返回对应的错误码。
     * @see OH_Cursor.
     * @since 10
     */
    int (*getRowCount)(OH_Cursor *cursor, int *count);

    /**
     * @brief 函数指针，转到结果集的下一行。
     *
     * @param cursor 表示指向{@link OH_Cursor}实例的指针。
     * @return 返回操作是否成功，出错时返回对应的错误码。
     * @see OH_Cursor.
     * @since 10
     */
    int (*goToNextRow)(OH_Cursor *cursor);

    /**
     * @brief 函数指针，当结果集中列的数据类型是BLOB或者TEXT时，获取其值所需的内存。
     *
     * @param cursor 表示指向{@link OH_Cursor}实例的指针。
     * @param columnIndex 表示结果集中指定列的索引, 索引值从0开始。
     * @param size 该参数是输出参数，BLOB或者TEXT数据所需内存大小会写入该变量。
     * @return 返回操作是否成功，出错时返回对应的错误码。
     * @see OH_Cursor.
     * @since 10
     */
    int (*getSize)(OH_Cursor *cursor, int32_t columnIndex, size_t *size);

    /**
     * @brief 函数指针，以字符串形式获取当前行中指定列的值。
     *
     * @param cursor 表示指向{@link OH_Cursor}实例的指针。
     * @param columnIndex 表示结果集中指定列的索引, 索引值从0开始。
     * @param value 该参数是输出参数，结果集中指定列的值会以字符串形式写入该变量。
     * @param length 表示value的长度，该值可通过getSize获取。
     * @return 返回操作是否成功，出错时返回对应的错误码。
     * @see OH_Cursor.
     * @since 10
     */
    int (*getText)(OH_Cursor *cursor, int32_t columnIndex, char *value, int length);

    /**
     * @brief 函数指针，以int64_t形式获取当前行中指定列的值。
     *
     * @param cursor 表示指向{@link OH_Cursor}实例的指针。
     * @param columnIndex 表示结果集中指定列的索引, 索引值从0开始。
     * @param value 该参数是输出参数，结果集中指定列的值会以int64_t形式写入该变量。
     * @return 返回操作是否成功，出错时返回对应的错误码。
     * @see OH_Cursor.
     * @since 10
     */
    int (*getInt64)(OH_Cursor *cursor, int32_t columnIndex, int64_t *value);

    /**
     * @brief 函数指针，以double形式获取当前行中指定列的值。
     *
     * @param cursor 表示指向{@link OH_Cursor}实例的指针。
     * @param columnIndex 表示结果集中指定列的索引, 索引值从0开始。
     * @param value 该参数是输出参数，结果集中指定列的值会以double形式写入该变量。
     * @return 返回操作是否成功，出错时返回对应的错误码。
     * @see OH_Cursor.
     * @since 10
     */
    int (*getReal)(OH_Cursor *cursor, int32_t columnIndex, double *value);

    /**
     * @brief 函数指针，以字节数组的形式获取当前行中指定列的值。
     *
     * @param cursor 表示指向{@link OH_Cursor}实例的指针。
     * @param columnIndex 表示结果集中指定列的索引, 索引值从0开始。
     * @param value 该参数是输出参数，结果集中指定列的值会以字节数组形式写入该变量。
     * @param length 表示value的长度，该值可通过getSize获取。
     * @return 返回操作是否成功，出错时返回对应的错误码。
     * @see OH_Cursor.
     * @since 10
     */
    int (*getBlob)(OH_Cursor *cursor, int32_t columnIndex, unsigned char *value, int length);

    /**
     * @brief 函数指针，检查当前行中指定列的值是否为null。
     *
     * @param cursor 表示指向{@link OH_Cursor}实例的指针。
     * @param columnIndex 表示结果集中指定列的索引, 索引值从0开始。
     * @param isNull 该参数是输出参数，如果当前行中指定列的值为null，该值为true，否则为false。
     * @return 返回操作是否成功，出错时返回对应的错误码。
     * @see OH_Cursor.
     * @since 10
     */
    int (*isNull)(OH_Cursor *cursor, int32_t columnIndex, bool *isNull);

    /**
     * @brief 函数指针，关闭结果集。
     *
     * @param cursor 表示指向{@link OH_Cursor}实例的指针。
     * @return 返回操作是否成功，出错时返回对应的错误码。
     * @see OH_Cursor.
     * @since 10
     */
    int (*destroy)(OH_Cursor *cursor);

    /**
     * @brief 函数指针，以资产的形式获取当前行中指定列的值。
     *
     * @param cursor 表示指向{@link OH_Cursor}实例的指针。
     * @param columnIndex 表示结果集中指定列的索引, 索引值从0开始。
     * @param value 该参数是输出参数，结果集中指定列的值会以资产形式写入该变量。
     * @return 返回操作是否成功，出错时返回对应的错误码。
     * @see OH_Cursor.
     * @since 11
     */
    int (*getAsset)(OH_Cursor *cursor, int32_t columnIndex, Data_Asset *value);

    /**
     * @brief 函数指针，以资产数组的形式获取当前行中指定列的值。
     *
     * @param cursor 表示指向{@link OH_Cursor}实例的指针。
     * @param columnIndex 表示结果集中指定列的索引, 索引值从0开始。
     * @param value 该参数是输出参数，结果集中指定列的值会以资产数组形式写入该变量。
     * @param length 表示资产数组的长度。
     * @return 返回操作是否成功，出错时返回对应的错误码。
     * @see OH_Cursor.
     * @since 11
     */
    int (*getAssets)(OH_Cursor *cursor, int32_t columnIndex, Data_Asset **value, uint32_t length);
} OH_Cursor;

#ifdef __cplusplus
};
#endif

#endif // OH_CURSOR_H
