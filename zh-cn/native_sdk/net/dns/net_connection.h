/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NATIVE_NET_CONN_API_H
#define NATIVE_NET_CONN_API_H

/**
 * @addtogroup NetConnection
 * @{
 *
 * @brief 为网络管理数据网络连接模块提供C接口。
 *
 * @since 11
 * @version 1.0
 */

/**
 * @file net_connection.h
 *
 * @brief 为网络管理数据网络连接模块提供C接口.
 *
 * @syscap SystemCapability.Communication.NetManager.Core
 * @library libnet_connection.so
 * @since 11
 * @version 1.0
 */

#include <netdb.h>

#include "net_connection_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 查询是否有默认激活的数据网络.
 *
 * @param hasDefaultNet 是否有默认网络.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.GET_NETWORK_INFO
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OH_NetConn_HasDefaultNet(int32_t *hasDefaultNet);

/**
 * @brief 获取激活的默认的数据网络.
 *
 * @param netHandle 存放网络ID.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.GET_NETWORK_INFO
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OH_NetConn_GetDefaultNet(NetConn_NetHandle *netHandle);

/**
 * @brief 查询默认数据网络是否记流量.
 *
 * @param isMetered 是否激活.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.GET_NETWORK_INFO
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OH_NetConn_IsDefaultNetMetered(int32_t *isMetered);

/**
 * @brief 查询某个数据网络的链路信息.
 *
 * @param nethandle 存放网络ID.
 * @param prop 存放链路信息.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.GET_NETWORK_INFO
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OH_NetConn_GetConnectionProperties(NetConn_NetHandle *netHandle, NetConn_ConnectionProperties *prop);

/**
 * @brief 查询某个网络的能力集.
 *
 * @param netHandle 存放网络ID.
 * @param netCapacities 存放能力集.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.GET_NETWORK_INFO
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OH_NetConn_GetNetCapabilities(NetConn_NetHandle *netHandle, NetConn_NetCapabilities *netCapacities);

/**
 * @brief 查询默认的网络代理.
 *
 * @param httpProxy 存放代理配置信息.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OH_NetConn_GetDefaultHttpProxy(NetConn_HttpProxy *httpProxy);

/**
 * @brief 通过netId获取DNS结果.
 *
 * @param host 所需查询的host名.
 * @param serv 服务名.
 * @param hint 指向addrinfo结构体的指针.
 * @param res 存放DNS查询结果，以链表形式返回.
 * @param netId DNS查询netId 为0是使用默认netid查询.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.INTERNET
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OH_NetConn_GetAddrInfo(char *host, char *serv, struct addrinfo *hint, struct addrinfo **res, int32_t netId);

/**
 * @brief 释放DNS结果.
 *
 * @param res DNS查询结果链表头.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.INTERNET
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OH_NetConn_FreeDnsResult(struct addrinfo *res);

/**
 * @brief 查询所有激活的数据网络.
 *
 * @param netHandleList 网络信息列表.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.GET_NETWORK_INFO
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OH_NetConn_GetAllNets(NetConn_NetHandleList *netHandleList);

/**
 * @brief 注册自定义 DNS 解析器.
 *
 * @param resolver 指向自定义 DNS 解析器的指针.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.INTERNET
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OHOS_NetConn_RegisterDnsResolver(OH_NetConn_CustomDnsResolver resolver);

/**
 * @brief 取消注册自定义 DNS 解析器.
 *
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.INTERNET
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OHOS_NetConn_UnregisterDnsResolver(void);

#ifdef __cplusplus
}
#endif

/** @} */
#endif /* NATIVE_NET_CONN_API_H */