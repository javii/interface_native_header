/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup image
 * @{
 *
 * @brief 提供获取pixelmap的数据和信息的接口方法。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 8
 * @version 1.0
 */

/**
 * @file image_pixel_map_napi.h
 *
 * @brief 声明可以锁定并访问pixelmap数据的方法，声明解锁的方法。
 *
 * @since 8
 * @version 1.0
 */

#ifndef INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_PIXEL_MAP_NAPI_H_
#define INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_PIXEL_MAP_NAPI_H_
#include <stdint>
#include "napi/native_api.h"
namespace OHOS {
namespace Media {
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 函数方法返回值的错误码的枚举。
 *
 * @deprecated since 10
 * @since 8
 * @version 1.0
 */
enum {
    /** 成功的结果 */
    OHOS_IMAGE_RESULT_SUCCESS = 0,
    /** 无效值 */
    OHOS_IMAGE_RESULT_BAD_PARAMETER = -1,
};

/**
 * @brief pixel 格式的枚举。
 *
 * @deprecated since 10
 * @since 8
 * @version 1.0
 */
enum {
    /**
     * 未知的格式
     */
    OHOS_PIXEL_MAP_FORMAT_NONE = 0,
    /**
     * 32-bit RGBA. 由 R, G, B组成，包括 A 都需要占用 8 bits。存储顺序是从高位到低位。
     */
    OHOS_PIXEL_MAP_FORMAT_RGBA_8888 = 3,
    /**
     * 16-bit RGB. 仅由 R, G, B 组成。
     * 存储顺序是从高位到低位: 红色占用5 bits，绿色占用6 bits，蓝色占用5 bits。
     */
    OHOS_PIXEL_MAP_FORMAT_RGB_565 = 2,
};

/**
 * @brief 用于定义 pixel map 的相关信息。
 *
 * @deprecated since 10
 * @since 8
 * @version 1.0
 */
struct OhosPixelMapInfo {
    /** 图片的宽, 用pixels表示 */
    uint32_t width;
    /** 图片的高, 用pixels表示 */
    uint32_t height;
    /** 每行的bytes数 */
    uint32_t rowSize;
    /** Pixel 的格式 */
    int32_t pixelFormat;
};

/**
 * @brief PixelMap 缩放类型的枚举。
 *
 * @since 10
 * @version 2.0
 */
enum {
    /**
     * 适应目标图片大小的格式
     */
    OHOS_PIXEL_MAP_SCALE_MODE_FIT_TARGET_SIZE = 0,
    /**
     * 以中心进行缩放的格式
     */
    OHOS_PIXEL_MAP_SCALE_MODE_CENTER_CROP = 1,
};

/**
 * @brief 获取 <b>PixelMap</b> 的信息，并记录信息到{@link OhosPixelMapInfo}结构中。
 *
 * @deprecated since 10
 * @param env napi的环境指针。
 * @param value 应用层的 <b>PixelMap</b> 对象。
 * @param info 用于保存信息的指针对象。 更多细节, 参看 {@link OhosPixelMapInfo}。
 * @return 如果获取并保存信息成功，则返回<b>0</b>; 如果操作失败，则返回错误码。
 * @see OhosPixelMapInfo
 * @since 8
 * @version 1.0
 */
int32_t OH_GetImageInfo(napi_env env, napi_value value, OhosPixelMapInfo *info);

/**
 * @brief 获取<b>PixelMap</b>对象数据的内存地址，并锁定该内存。
 *
 * 函数执行成功后，<b>*addrPtr</b>就是获取的待访问的内存地址。访问操作完成后，必须要使用{@link OH_UnAccessPixels}来释放锁，否则的话资源无法被释放。
 * 待解锁后，内存地址就不可以再被访问和操作。
 *
 * @deprecated since 10
 * @param env napi的环境指针。
 * @param value 应用层的 <b>PixelMap</b> 对象。
 * @param addrPtr 用于指向的内存地址的双指针对象。
 * @see UnAccessPixels
 * @return 操作成功则返回 {@link OHOS_IMAGE_RESULT_SUCCESS}；如果操作失败，则返回错误码。
 * @since 8
 * @version 1.0
 */
int32_t OH_AccessPixels(napi_env env, napi_value value, void** addrPtr);

/**
 * @brief 释放<b>PixelMap</b>对象数据的内存锁, 用于匹配方法{@link OH_AccessPixels}。
 *
 * @deprecated since 10
 * @param env napi的环境指针。
 * @param value 应用层的 <b>PixelMap</b> 对象。
 * @return 操作成功则返回 {@link OHOS_IMAGE_RESULT_SUCCESS}；如果操作失败，则返回错误码。
 * @see AccessPixels
 * @since 8
 * @version 1.0
 */
int32_t OH_UnAccessPixels(napi_env env, napi_value value);

#ifdef __cplusplus
};
#endif
/** @} */
} // namespace Media
} // namespace OHOS
#endif // INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_PIXEL_MAP_NAPI_H_
