/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef QOS_H
#define QOS_H
/**
 * @addtogroup QoS
 * @{
 *
 * @brief 提供QoS接口，包括设置、取消和查询QoS等级。
 *
 * @since 12
 */

/**
 * @file qos.h
 *
 * @brief 声明QoS提供的C接口。
 *
 * @syscap SystemCapability.Resourceschedule.QoS.Core
 *
 * @since 12
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 描述QoS等级。
 *
 * @since 12
 */
typedef enum QoS_Level {
    /**
     * @brief 表示QoS等级为用户不可见任务。
     */
    QOS_BACKGROUND = 0,

    /**
     * @brief 表示QoS等级为后台重要任务。
     */
    QOS_UTILITY,

    /**
     * @brief 表示QoS等级为默认。
     */
    QOS_DEFAULT,

    /**
     * @brief 表示QoS等级为用户触发任务。
     */
    QOS_USER_INITIATED,

    /**
     * @brief 表示QoS等级为限时任务。
     */
    QOS_DEADLINE_REQUEST,

    /**
     * @brief 表示QoS等级为用户交互任务。
     */
    QOS_USER_INTERACTIVE,
} QoS_Level;

/**
 * @brief 为当前线程设置QoS等级。
 *
 * @param level 表示设置的QoS等级，详细信息可以查看{@link QoS_Level}。
 * @return 成功返回0，失败返回负值。
 * @see QoS_Level
 * @since 12
 */
int OH_QoS_SetThreadQoS(QoS_Level level);

/**
 * @brief 取消当前线程的QoS等级。
 *
 * @return 成功返回0，失败返回负值。
 * @see QoS_Level
 * @since 12
 */
int OH_QoS_ResetThreadQoS();

/**
 * @brief 获得当前线程的QoS等级。
 *
 * @param level 参数是输出参数，线程的QoS等级会以{@link QoS_Level}形式写入该变量。
 * @return 成功返回0，失败返回负值。
 * @see QoS_Level
 * @since 12
 */
int OH_QoS_GetThreadQoS(QoS_Level *level);
#ifdef __cplusplus
}
#endif
/** @} */
#endif // QOS_H