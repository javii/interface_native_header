/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Hostapd
 * @{
 *
 * @brief 定义上层WLAN服务的API接口。
 *
 * 上层WLAN服务开发人员可根据WLAN模块提供的向上统一接口获取如下能力：建立/关闭WLAN热点，扫描/关联WLAN热点，WLAN平台芯片管理，网络数据缓冲的申请、释放、移动等操作，网络设备管，电源管理等。
 *
 * @since 4.1
 * @version 1.0
 */

/**
 * @file IHostapdInterface.idl
 *
 * @brief 提供API接口，实现WLAN热点的开启、关闭、扫描、连接、断开等功能，设置国家代码，管理网络设备。
 *
 * 模块包路径：ohos.hdi.wlan.hostapd.v1_0
 *
 * 引用：
 * - ohos.hdi.wlan.hostapd.v1_0.HostapdTypes
 * - ohos.hdi.wlan.hostapd.v1_0.IHostapdCallback
 *
 * @since 4.1
 * @version 1.0
 */



package ohos.hdi.wlan.hostapd.v1_0;

import ohos.hdi.wlan.hostapd.v1_0.HostapdTypes;
import ohos.hdi.wlan.hostapd.v1_0.IHostapdCallback;

/**
 * @brief 定义上层WLAN服务的接口。
 *
 * @since 4.1
 * @version 1.0
 */

interface IHostapdInterface {
    /**
     * @brief 打开AP。
     *
     * @param ifName 表示网卡名称。
     * @param id 表示热点id。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.0
     */
    StartAp();

    /**
     * @brief 关闭AP。
     *
     * @param ifName 表示网卡名称。
     * @param id 表示热点id。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.0
     */
    StopAp();

    /**
     * @brief 启用AP。
     *
     * @param ifName 表示网卡名称。
     * @param id 表示热点id。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.0
     */
    EnableAp([in] String ifName, [in] int id);

    /**
     * @brief 禁用AP。
     *
     * @param ifName 表示网卡名称。
     * @param id 表示热点id。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.0
     */
    DisableAp([in] String ifName, [in] int id);

    /**
     * @brief 设置个人热点密码。
     *
     * @param ifName 表示网卡名称。
     * @param pass 密码。
     * @param id 表示热点id。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.0
     */
    SetApPasswd([in] String ifName, [in] String pass, [in]int id);

    /**
     * @brief 设置个人热点名称。
     *
     * @param ifName 表示网卡名称。
     * @param name 表示要设置的热点名称。
     * @param id 表示热点id。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.0
     */
    SetApName([in] String ifName, [in] String name, [in] int id);

    /**
     * @brief 设置AP安全类型。
     *
     * @param ifName 表示网卡名称。
     * @param securityType SAP安全类型，例如：wpa/wpa_psk等。
     * @param id 表示热点id。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.0
     */
    SetApWpaValue([in] String ifName, [in] int securityType, [in] int id);

    /**
     * @brief 设置AP带宽。
     *
     * @param ifName 表示网卡名称。
     * @param band 表示SAP带宽。
     * @param id 表示热点id。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.0
     */
    SetApBand([in] String ifName, [in] int band, [in] int id);

    /**
     * @brief 设置AP需要支持的协议类型。
     *
     * @param ifName 表示网卡名称。
     * @param value 表示Hostapd配置值。
     * @param id 表示热点id。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.0
     */
    SetAp80211n([in] String ifName, [in] int value, [in] int id);

    /**
     * @brief 设置AP WMM模式。
     *
     * @param ifName 表示网卡名称。
     * @param value 表示启用或禁用Wmm。
     * @param id 表示热点id。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.0
     */
    SetApWmm([in] String ifName, [in] int value, [in] int id);

    /**
     * @brief 设置AP通道。
     *
     * @param ifName 表示网卡名称。
     * @param channel 表示SAP通道。
     * @param id 表示热点id。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.0
     */
    SetApChannel([in] String ifName, [in] int channel, [in] int id);

    /**
     * @brief 设置AP最大连接。
     *
     * @param ifName 表示网卡名称。
     * @param maxConn 表示设置连接设备的最大数量。
     * @param id 表示热点id。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.0
     */
    SetApMaxConn([in] String ifName, [in] int maxConn, [in] int id);

    /**
     * @brief 设置AP模式下的黑名单设置为禁止MAC地址连接。
     *
     * @param ifName 表示网卡名称。
     * @param mac 表示被阻止的MAC地址。
     * @param id 表示热点id。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.0
     */
    SetMacFilter([in] String ifName, [in] String mac, [in] int id);

    /**
     * @brief 在AP模式下设置的黑名单过滤，并删除来自黑名单中指定的MAC地址。
     *
     * @param ifName 表示网卡名称。
     * @param mac 表示黑名单中的MAC地址。
     * @param id 表示热点id。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.0
     */
    DelMacFilter([in] String ifName, [in] String mac, [in] int id);

    /**
     * @brief 获取有关所有连接的STA的信息。
     *
     * @param ifName 表示网卡名称。
     * @param infos 表示已连接STA数组信息。
     * @param size 表示获取已连接STA数组中，数组信息的大小。
     * @param id 表示热点id。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.0
     */
    GetStaInfos([in] String ifName, [out] String buf, [in] int size, [in] int id);

    /**
     * @brief 断开指定的STA连接。
     *
     * @param ifName 表示网卡名称。
     * @param mac 表示断开指定的mac。
     * @param id 表示热点id。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.0
     */
    DisassociateSta([in] String ifName, [in] String mac, [in] int id);

    /**
     * @brief 注册回调以侦听异步事件。
     *
     * @param cbFunc 表示要注册的回调。
     * @param ifName 表示网卡名称。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.0
     */
    RegisterEventCallback([in] IHostapdCallback cbFunc, [in] String ifName);

    /**
     * @brief 注销回调。
     *
     * @param cbFunc 表示要注销的回调。
     * @param ifName 表示网卡名称。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.0
     */
    UnregisterEventCallback([in] IHostapdCallback cbFunc, [in] String ifName);

     /**
     * @brief 用于处理Hostapd的cmd命令。
     *
     * @param ifName 表示网卡名称。
     * @param cmd 表示来自WifiHal的HostApd命令。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则为负值。
     *
     * @since 4.1
     * @version 1.0
     */
     HostApdShellCmd([in] String ifName, [in] String cmd);
 }
/** @} */