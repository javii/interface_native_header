/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Distributed Audio
 * @{
 *
 * @brief Distributed Audio
 *
 * Distributed Audio模块包括对分布式音频设备的操作、流的操作和各种回调等。
 * 通过IDAudioCallback和IDAudioManager接口，与Source SA通信交互，实现分布式功能。
 *
 * @since 4.1
 * @version 1.0
 */

/**
 * @file IDAudioManager.idl
 *
 * @brief 分布式音频HDF层的调用接口，为分布式音频SA提供硬件驱动注册、去注册以及事件通知能力。
 *
 * 模块包路径：ohos.hdi.distributed_audio.audioext.v1_0
 *
 * 引用：
 * - ohos.hdi.distributed_audio.audioext.v1_0.IDAudioCallback
 * - ohos.hdi.distributed_audio.audioext.v1_0.IDAudioCallback
 *
 * @since 4.1
 * @version 1.0
 */


package ohos.hdi.distributed_audio.audioext.v1_0;

import ohos.hdi.distributed_audio.audioext.v1_0.IDAudioCallback;
import ohos.hdi.distributed_audio.audioext.v1_0.Types;

/**
 * @brief 定义Distributed Audio设备基本的操作。
 *
 * 注册与去注册分布式音频设备、提供分布式音频SA向HDF层的事件通知机制。
 *
 * @since 4.1
 * @version 1.0
 */
interface IDAudioManager {
    /**
     * @brief 注册分布音频设备驱动。
     *
     * @param adpName 分布式音频设备NetworkID。
     * @param devId 分布式音频设备的端口ID。
     * @param capability 分布式音频设备能力集（包括采样率、通道数等）。
     * @param callbackObj 分布式音频SA回调。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    RegisterAudioDevice([in] String adpName, [in] int devId, [in] String capability, [in] IDAudioCallback callbackObj);
    /**
     * @brief 去注册分布音频设备驱动。
     *
     * @param adpName 分布式音频设备NetworkID。
     * @param devId 分布式音频设备的端口ID。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    UnRegisterAudioDevice([in] String adpName, [in] int devId);
    /**
     * @brief 分布音频设备SA通知事件。
     *
     * @param adpName 分布式音频设备NetworkID。
     * @param devId 分布式音频设备的端口ID。
     * @param event 通知事件类型（如焦点事件，音量事件）。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.1
     * @version 1.0
     */
    NotifyEvent([in] String adpName, [in] int devId, [in] struct DAudioEvent event);
}
/** @} */