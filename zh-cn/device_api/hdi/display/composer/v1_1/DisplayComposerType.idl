/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给上层图形服务使用的驱动接口，包括图层管理、设备控制、显示内存管理等相关接口。
 *
 * @since 4.1
 * @version 1.1
 */

/**
 * @file DisplayComposerType.idl
 *
 * @brief 显示合成类型定义，定义显示图层合成操作相关接口所使用的数据类型。
 *
 * 模块包路径：ohos.hdi.display.composer.v1_1
 *
 * 引用：ohos.hdi.display.composer.v1_0.DisplayComposerType
 *
 * @since 4.1
 * @version 1.1
 */


package ohos.hdi.display.composer.v1_1;
sequenceable OHOS.HDI.Display.HdifdParcelable;
import ohos.hdi.display.composer.v1_0.DisplayComposerType;

/**
 * @brief 像素格式类型定义。
 *
 * @since 4.1
 * @version 1.1
 */
enum PixelFormat : ohos.hdi.display.composer.v1_0.PixelFormat {
    PIXEL_FMT_YCBCR_P010 = 35, /**< YCBCR420 半平面 10 位格式。 */
    PIXEL_FMT_YCRCB_P010,      /**< YCRCB420 半平面 10 位格式。 */
    PIXEL_FMT_RAW10,           /**< RAW 10bit 格式。 */
};

/**
 * @brief 枚举显示状态。
 *
 * @since 4.1
 * @version 1.1
 */
enum DispPowerStatus : ohos.hdi.display.composer.v1_0.DispPowerStatus {
    POWER_STATUS_OFF_FAKE = 4,      /**< 当 hwc 关闭时，电源状态为 ON。 */
    POWER_STATUS_BUTT_V1_1,         /**< 电源状态无效。 */
};

/**
 * @brief 枚举特殊层的组合类型。
 *
 * @since 4.1
 * @version 1.1
 */
enum CompositionType : ohos.hdi.display.composer.v1_0.CompositionType {
    COMPOSITION_SOLID_COLOR = 7,    /**< Tunnel 组合类型，用于 tunnel。 */
    COMPOSITION_BUTT_V1_1,          /**< 无效的合成类型。 */
};

/**
 * @brief 定义输出模式 ext 信息。
 *
 * @since 4.1
 * @version 1.1
 */
struct DisplayModeInfoExt {
    struct DisplayModeInfo v1_0; /**< 显示合成类型定义，定义显示图层合成操作相关接口所使用的数据类型。 */
    unsigned int groupId;        /**< ltpo 序号。 */
};
/** @} */