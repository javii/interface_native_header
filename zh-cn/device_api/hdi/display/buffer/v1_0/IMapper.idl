/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给上层图形服务使用的驱动接口，包括图层管理、设备控制、显示内存管理等相关接口。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IMapper.idl
 *
 * @brief 显示内存映射接口声明。
 *
 * 模块包路径：ohos.hdi.display.buffer.v1_0
 *
 * 引用：ohos.hdi.display.buffer.v1_0.DisplayBufferType
 *
 * @since 3.2
 * @version 1.0
 */


package ohos.hdi.display.buffer.v1_0;

import ohos.hdi.display.buffer.v1_0.DisplayBufferType;

/**
 * @brief 定义释放显示内存接口。
 *
 * @since 3.2
 * @version 1.0
 */
interface IMapper {
    /**
     * @brief 释放显示内存。
     *
     * @param handle 待释放的内存handle指针。
     *
     * @return 返回0 表示执行成功。
     * @return 返回其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 3.2
     * @version 1.0
     */
    FreeMem([in] NativeBuffer handle);

    /**
     * @brief 显示内存映射，将内存映射到对应的进程地址空间中。
     *
     * @param handle 待映射内存handle指针。
     *
     * @return 返回0 表示执行成功。
     * @return 返回其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 3.2
     * @version 1.0
     */
    Mmap([in] NativeBuffer handle);

    /**
     * @brief 内存反映射，将内存进行反映射。
     *
     * @param handle 待反映射内存handle指针。
     *
     * @return 返回0 表示执行成功。
     * @return 返回其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 3.2
     * @version 1.0
     */
    Unmap([in] NativeBuffer handle);

    /**
     * @brief 刷新Cache，刷新Cache里的内容到内存并且使Cache里的内容无效。
     *
     * @param handle 待刷新Cache的handle指针。
     *
     * @return 返回0 表示执行成功。
     * @return 返回其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 3.2
     * @version 1.0
     */
    FlushCache([in] NativeBuffer handle);

    /**
     * @brief 使cache中的内容无效用以存储更新内存内容。
     *
     * @param  handle 待无效cache的handle指针。
     *
     * @return 返回0 表示执行成功。
     * @return 返回其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 3.2
     * @version 1.0
     */
    InvalidateCache([in] NativeBuffer handle);
}
/** @} */