/**
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiA2dp
 * @{
 *
 * @brief HdiA2dp为A2DP服务提供统一接口。
 *
 * 主机可以通过该模块提供的接口创建音频通话，与音频子系统交换数据。
 *
 * @since 4.0
 */

/**
 * @file IBluetoothAudioSession.idl
 *
 * @brief 声明开启音频会话，发送渲染操作结果，和结束音频会话的接口。
 *
 * 模块包路径：ohos.hdi.bluetooth.a2dp.v1_0
 *
 * 引用：
 * - ohos.hdi.bluetooth.a2dp.v1_0.IBluetoothAudioCallback
 * - ohos.hdi.bluetooth.a2dp.v1_0.BluetoothAudioTypes
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.bluetooth.a2dp.v1_0;

import ohos.hdi.bluetooth.a2dp.v1_0.IBluetoothAudioCallback;
import ohos.hdi.bluetooth.a2dp.v1_0.BluetoothAudioTypes;

/**
 * @brief 声明开启音频会话，发送渲染操作结果，和结束音频会话的接口。
 *
 * @since 4.0
 */
interface IBluetoothAudioSession {
    /**
     * @brief 开启音频会话并注册回调函数。
     *
     * @param sessionType 表示会话类型。
     * @param callbackObj 表示回调函数。相关详细信息，请参考{@link IBluetoothAudioCallback}。
     * @param queue 返回音频数据的队列。
     *
     * @return 如果操作成功返回0；否则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    StartSession([in] enum SessionType sessionType, [in] IBluetoothAudioCallback callbackObj,
        [out] SharedMemQueue<unsigned char> queue);

    /**
     * @brief 结束音频会话。
     *
     * @param SessionType 表示会话类型。
     * @return 如果操作成功返回0；否则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    StopSession([in] enum SessionType sessionType);

    /**
     * @brief 发送渲染操作结果。
     *
     * @param operation 表示渲染操作。
     * @param Status 表示渲染操作成功或失败。
     * @return 如果操作成功返回0；否则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    RenderOperationResult([in] enum Operation operation, [in] enum Status status);
}
/** @} */