/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup IntelligentVoiceEngine
 * @{
 *
 * @brief IntelligentVoiceEngine模块向上层服务提供了统一接口。
 *
 * 上层服务开发人员可根据IntelligentVoiceEngine模块提供的向上统一接口获取如下能力：创建销毁唤醒算法引擎、启动停止唤醒算法引擎、写语音数据、读文件、回调函数注册等。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file IIntellVoiceEngineManager.idl
 *
 * @brief IntelligentVoiceEngine模块引擎管理接口，包括获取引擎适配器描述符、创建引擎适配器、释放引擎适配器等。
 *
 * 模块包路径：ohos.hdi.intelligent_voice.engine.v1_0
 *
 * 引用：
 * - ohos.hdi.intelligent_voice.engine.v1_0.IntellVoiceEngineTypes
 * - ohos.hdi.intelligent_voice.engine.v1_0.IIntellVoiceEngineAdapter
 * - ohos.hdi.intelligent_voice.engine.v1_0.IIntellVoiceEngineCallback
 *
 * @since 4.0
 * @version 1.0
 */


package ohos.hdi.intelligent_voice.engine.v1_0;

import ohos.hdi.intelligent_voice.engine.v1_0.IntellVoiceEngineTypes;
import ohos.hdi.intelligent_voice.engine.v1_0.IIntellVoiceEngineAdapter;
import ohos.hdi.intelligent_voice.engine.v1_0.IIntellVoiceDataOprCallback;

 /**
 * @brief IntelligentVoiceEngine模块向上层服务提供了智能语音引擎管理接口。
 *
 * 上层服务开发人员可根据IntelligentVoiceEngine模块提供的向上智能语音引擎管理接口实现获取引擎适配器描述符、创建引擎适配器、释放引擎适配器等功能。
 *
 * @since 4.0
 * @version 1.0
 */
interface IIntellVoiceEngineManager {
    /**
     * @brief 上层服务查询智能语音引擎适配器描述符。
     *
     * @param descs 存放智能语音引擎适配器描述符的数组，信息包含智能语音引擎适配器类型，具体参考{@link IntellVoiceEngineAdapterDescriptor}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    GetAdapterDescriptors([out] List <struct IntellVoiceEngineAdapterDescriptor> descs);
    /**
     * @brief 上层服务创建智能语音引擎适配器。
     *
     * @param descriptor 智能语音引擎适配器描述符，信息包含智能语音引擎适配器类型，具体参考{@link IntellVoiceEngineAdapterDescriptor}。
     * @param adapter 智能语音引擎适配器，具体参考{@link IIntellVoiceEngineAdapter}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    CreateAdapter([in] struct IntellVoiceEngineAdapterDescriptor descriptor, [out] IIntellVoiceEngineAdapter adapter);
    /**
     * @brief 上层服务释放智能语音引擎适配器。
     *
     * @param descriptor 智能语音引擎适配器描述符，信息包含智能语音引擎适配器类型，具体参考{@link IntellVoiceEngineAdapterDescriptor}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    ReleaseAdapter([in] struct IntellVoiceEngineAdapterDescriptor descriptor);
}
/** @} */