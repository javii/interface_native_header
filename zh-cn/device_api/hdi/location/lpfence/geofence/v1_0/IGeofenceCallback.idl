/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup HdiLpfenceGeofence
 * @{
 *
 * @brief 为低功耗围栏服务提供地理围栏的API。
 *
 * 本模块接口提供添加圆形和多边形地理围栏，删除地理围栏，获取地理围栏状态信息，获取设备地理位置等功能。本模块可在AP休眠状态下持续工作。
 *
 * 应用场景：判断用户设备是否达到某个精确地理位置区域，从而进行一些后续服务，如门禁卡的切换、定制消息的提醒等。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file IGeofenceCallback.idl
 *
 * @brief 定义地理围栏模块回调接口。
 *
 * 模块包路径：ohos.hdi.location.lpfence.geofence.v1_0
 *
 * 引用：ohos.hdi.location.lpfence.geofence.v1_0.GeofenceTypes
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.location.lpfence.geofence.v1_0;

import ohos.hdi.location.lpfence.geofence.v1_0.GeofenceTypes;

/**
 * @brief 定义地理围栏模块的回调函数。
 *
 * 用户在开启地理围栏功能前，需要先注册该回调函数。当地理围栏状态发生变化时，会通过回调函数进行上报。
 * 详情可参考{@link ICellfenceInterface}。 
 *
 * @since 4.0
 */
[callback] interface IGeofenceCallback {
    /**
     * @brief 定义添加地理围栏结果的回调函数。
     *
     * 对每个地理围栏的添加结果，通过该回调函数进行上报。
     *
     * @param res 上报地理围栏添加结果。详见{@link GeofenceResult}定义。
     *
     * @return 如果调用成功，则返回0。
     * @return 如果调用失败，则返回负值。
     *
     * @since 4.0
     */
    OnGeofenceAddResultCb([in] struct GeofenceResult[] res);

    /**
     * @brief 定义删除地理围栏结果的回调函数。
     *
     * 对每个地理围栏的删除结果，通过该回调函数进行上报。
     *
     * @param res 上报地理围栏删除结果。详见{@link GeofenceResult}定义。
     *
     * @return 如果调用成功，则返回0。
     * @return 如果调用失败，则返回负值。
     *
     * @since 4.0
     */
    OnGeofenceRemoveResultCb([in] struct GeofenceResult[] res);

    /**
     * @brief 定义地理围栏状态变化的回调函数。
     *
     * 设备与地理围栏的状态关系发生变化时，会通过该回调函数进行上报。
     *
     * @param geofenceId 地理围栏id号。
     * @param location 最新的位置坐标。详见{@link GeoLocationInfo}定义。
     * @param transition 地理围栏变化的状态。详见{@link GeofenceTransition}定义。
     * @param timeStamp 时间戳。
     *
     * @return 如果调用成功，则返回0。
     * @return 如果调用失败，则返回负值。
     *
     * @since 4.0
     */
    OnGeofenceTransitionCb([in] int geofenceId,
                           [in] struct GeoLocationInfo location,
                           [in] unsigned char transition,
                           [in] long timeStamp);

    /**
     * @brief 定义地理围栏使用信息的回调函数。
     *
     * 获取地理围栏使用信息时，会通过该回调函数进行上报。
     *
     * @param size 地理围栏使用信息。详见{@link GeofenceSize}定义。
     *
     * @return 如果调用成功，则返回0。
     * @return 如果调用失败，则返回负值。
     *
     * @since 4.0
     */
    OnGetGeofenceSizeCb([in] struct GeofenceSize size);

    /**
     * @brief 定义请求基站离线数据库的回调函数。
     *
     * 设备请求基站离线数据库时，会通过该回调函数进行上报。
     *
     * @param req 请求基站离线数据库数据结构。详见{@link RequestCellDb}定义。
     *
     * @return 如果调用成功，则返回0。
     * @return 如果调用失败，则返回负值。
     *
     * @since 4.0
     */
    OnGeofenceRequestCellDb([in] struct RequestCellDb req);

    /**
     * @brief 定义获取设备最新位置信息的回调函数。
     *
     * 请求获取设备最新位置信息时，会通过该回调函数进行上报。
     *
     * @param locSource 位置信息来源。详见{@link GeofenceLocSource}定义。
     * @param location 最新位置信息。详见{@link GeoLocationInfo}定义。
     *
     * @return 如果调用成功，则返回0。
     * @return 如果调用失败，则返回负值。
     *
     * @since 4.0
     */
    OnGetCurrentLocation([in] int locSource,
                         [in] struct GeoLocationInfo location);

    /**
     * @brief 定义低功耗围栏服务复位事件通知的回调函数。
     *
     * 低功耗围栏服务发生复位时会通过该回调函数进行事件上报。
     *
     * @return 如果回调函数调用成功，则返回0。
     * @return 如果回调函数调用失败，则返回负值。
     *
     * @since 4.0
     */
    OnGeofenceReset();
}
/** @} */