/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiGnss
 * @{
 *
 * @brief 定义GNSS模块的接口。
 *
 * 上层GNSS服务可以获取GNSS驱动对象或代理，然后调用该对象或代理提供的API来访问GNSS设备，
 * 从而实现启动GNSS芯片，启动导航，设置GNSS工作模式，注入参考信息，获取定位结果，获取nmea，
 * 获取卫星状态信息，批量获取缓存位置信息等。
 *
 * @since 3.2
 */

/**
 * @file IGnssCallback.idl
 *
 * @brief 声明获取定位结果回调、获取GNSS模块工作状态回调、获取nmea回调、获取GNSS能力回调、
 * 获取卫星状态信息回调、批量获取缓存位置回调、请求上层注入参考信息回调、
 * 请求上层注入PGNSS数据回调。
 *
 * 模块包路径：ohos.hdi.location.gnss.v1_0
 *
 * 引用：ohos.hdi.location.gnss.v1_0.GnssTypes
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.location.gnss.v1_0;

import ohos.hdi.location.gnss.v1_0.GnssTypes;

/**
 * @brief 声明获取定位结果回调、获取GNSS模块工作状态回调、获取nmea回调、获取GNSS能力回调、
 * 获取卫星状态信息回调、批量获取缓存位置回调、请求上层注入参考信息回调、
 * 请求上层注入PGNSS数据回调。
 *
 * @since 3.2
 */
[callback] interface IGnssCallback {
    /**
     * @brief 位置上报的回调函数。
     *
     * @param location 表示GNSS定位结果，详情参考{@link Location}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    ReportLocation([in] struct LocationInfo location);

    /**
     * @brief 上报GNSS工作状态的回调函数。
     *
     * @param status 表示GNSS芯片的工作状态，详情参考{@link GnssWorkingStatus}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    ReportGnssWorkingStatus([in] enum GnssWorkingStatus status);

    /**
     * @brief 上报NMEA数据的回调函数。
     *
     * @param timestamp 表示NMEA上报的时刻。
     * @param nmea 表示NMEA字符串。格式是NMEA 0183。
     * @param length 表示NMEA字符串的长度。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    ReportNmea([in] long timestamp, [in] String nmea, [in] int length);

    /**
     * @brief 上报GNSS能力的回调函数。
     *
     * @param capabilities 表示GNSS的能力。详情参考{@link GnssCapabilities}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    ReportGnssCapabilities([in] enum GnssCapabilities capabilities);

    /**
     * @brief 上报卫星状态信息的回调函数。
     *
     * @param info 表示卫星状态信息，详情参考{@link SatelliteStatusInfo}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    ReportSatelliteStatusInfo([in] struct SatelliteStatusInfo info);

    /**
     * @brief 请求上层注入GNSS参考信息。
     *
     * @param type 表示GNSS参考信息类型，详情参考{@link GnssRefInfoType}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    RequestGnssReferenceInfo([in] enum GnssRefInfoType type);

    /**
     * @brief 请求上层注入PGNSS数据。
     *
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    RequestPredictGnssData();

    /**
     * @brief 批量上报所有的缓存GNSS位置信息。
     *
     * @param gnssLocations 表示GNSS芯片缓存的所有位置信息。详情参考{@link Location}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    ReportCachedLocation([in] struct LocationInfo[] gnssLocations);
}
/** @} */