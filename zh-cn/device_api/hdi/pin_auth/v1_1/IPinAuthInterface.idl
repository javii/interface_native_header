/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfPinAuth
 * @{
 *
 * @brief 提供口令认证驱动的标准API接口。
 *
 * 口令认证驱动为口令认证服务提供统一的访问接口。获取口令认证驱动代理后，口令认证服务可以调用相关接口获取执行器，获取口令认证执行器后，
 * 口令认证服务可以调用相关接口获取执行器信息，获取凭据模版信息，注册口令，认证口令，删除口令等。
 *
 * @since 4.0
 */

/**
 * @file IPinAuthInterface.idl
 *
 * @brief 定义获取口令认证驱动的执行器列表接口，用于从口令认证驱动获取执行器对象列表。
 *
 * 模块包路径：ohos.hdi.pin_auth.v1_1
 *
 * 引用：
 * - ohos.hdi.pin_auth.v1_0.IPinAuthInterface
 * - ohos.hdi.pin_auth.v1_1.IExecutor
 *
 * @since 4.0
 */

package ohos.hdi.pin_auth.v1_1;

import ohos.hdi.pin_auth.v1_0.IPinAuthInterface;
import ohos.hdi.pin_auth.v1_1.IExecutor;

/**
 * @brief 定义获取口令认证驱动的执行器列表接口。
 *
 * @since 4.0
 * @version 1.1
 */
interface IPinAuthInterface extends ohos.hdi.pin_auth.v1_0.IPinAuthInterface {
    /**
     * @brief 获取执行器列表，口令认证服务进程启动进行初始化操作时通过该接口获取口令认证驱动支持的执行器列表。
     *
     * @param executorList 执行器对象列表{@link IExecutor}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 4.0
     * @version 1.1
     */
    GetExecutorListV1_1([out] IExecutor[] executorList);
}
/** @} */