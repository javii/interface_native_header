/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_TEXT_BLOB_H
#define C_INCLUDE_DRAWING_TEXT_BLOB_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module does not provide the pixel unit. The pixel unit to use is consistent with the application context
 * environment. In the ArkUI development environment, the default pixel unit vp is used.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_text_blob.h
 *
 * @brief Declares the functions related to the text blob in the drawing module.
 *
 * File to include: native_drawing/drawing_text_blob.h
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Creates an <b>OH_Drawing_TextBlobBuilder</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_TextBlobBuilder</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_TextBlobBuilder* OH_Drawing_TextBlobBuilderCreate(void);

/**
 * @brief Creates an <b>OH_Drawing_TextBlob</b> object from the text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param text Pointer to the text.
 * @param byteLength Length of the text, in bytes.
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @param OH_Drawing_TextEncoding Encoding type of the text.
 * For details about the available options, see {@link OH_Drawing_TextEncoding}.
 * @return Returns the pointer to the {@link OH_Drawing_TextBlob} object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextBlob* OH_Drawing_TextBlobCreateFromText(const void* text, size_t byteLength,
    const OH_Drawing_Font*, OH_Drawing_TextEncoding);

/**
 * @brief Creates an <b>OH_Drawing_TextBlob</b> object from the text and text position.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param text Pointer to the text.
 * @param byteLength Length of the text, in bytes.
 * @param OH_Drawing_Point2D Pointer to the start address of an {@link OH_Drawing_Point2D} array.
 * The number of entries in the array is the value obtained by calling {@link OH_Drawing_FontCountText}.
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @param OH_Drawing_TextEncoding Encoding type of the text.
 * For details about the available options, see {@link OH_Drawing_TextEncoding}.
 * @return Returns the pointer to the {@link OH_Drawing_TextBlob} object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextBlob* OH_Drawing_TextBlobCreateFromPosText(const void* text, size_t byteLength,
    OH_Drawing_Point2D*, const OH_Drawing_Font*, OH_Drawing_TextEncoding);

/**
 * @brief Creates an <b>OH_Drawing_TextBlob</b> object from a string.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param str Pointer to a string.
 * @param OH_Drawing_Font Pointer to an {@link OH_Drawing_Font} object.
 * @param OH_Drawing_TextEncoding Encoding type of the text.
 * For details about the available options, see {@link OH_Drawing_TextEncoding}.
 * @return Returns the pointer to the {@link OH_Drawing_TextBlob} object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextBlob* OH_Drawing_TextBlobCreateFromString(const char* str,
    const OH_Drawing_Font*, OH_Drawing_TextEncoding);

/**
 * @brief Obtains the bounds of an <b>OH_Drawing_TextBlob</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBlobPointer to an {@link OH_Drawing_TextBlob} object.
 * @param OH_Drawing_Rect Pointer to an {@link OH_Drawing_Rect} object,
 * which is obtained by calling {@link OH_Drawing_Rect}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextBlobGetBounds(OH_Drawing_TextBlob*, OH_Drawing_Rect*);

/**
 * @brief Describes a run, which provides storage for glyphs and positions.
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_Drawing_RunBuffer {
    /** Storage for glyph indexes in the run. */
    uint16_t* glyphs;
    /** Storage for glyph positions in the run. */
    float* pos;
    /** Storage for UTF-8 encoded text units in the run. */
    char* utf8text;
    /** Storage for glyph clusters (index of the UTF-8 encoded text unit) in the run. */
    uint32_t* clusters;
} OH_Drawing_RunBuffer;

/**
 * @brief Allocates a run to store glyphs and positions. The pointer returned does not need to be managed by the caller.
 * It can no longer be used after {@link OH_Drawing_TextBlobBuilderMake} is called.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBlobBuilder Pointer to an <b>OH_Drawing_TextBlobBuilder</b> object.
 * @param OH_Drawing_Font Pointer to an <b>OH_Drawing_Font</b> object.
 * @param count Number of text blobs.
 * @param OH_Drawing_Rect Rectangle of the text blob.
 * @since 11
 * @version 1.0
 */
const OH_Drawing_RunBuffer* OH_Drawing_TextBlobBuilderAllocRunPos(OH_Drawing_TextBlobBuilder*, const OH_Drawing_Font*,
    int32_t count, const OH_Drawing_Rect*);

/**
 * @brief Makes an <b>OH_Drawing_TextBlob</b> object from an <b>OH_Drawing_TextBlobBuilder</b>.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBlobBuilder Pointer to an <b>OH_Drawing_TextBlobBuilder</b> object.
 * @return Returns the pointer to the <b>OH_Drawing_TextBlob</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_TextBlob* OH_Drawing_TextBlobBuilderMake(OH_Drawing_TextBlobBuilder*);

/**
 * @brief Destroys an <b>OH_Drawing_TextBlob</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBlobPointer to an <b>OH_Drawing_TextBlob</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_TextBlobDestroy(OH_Drawing_TextBlob*);

/**
 * @brief Destroys an <b>OH_Drawing_TextBlobBuilder</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBlobBuilder Pointer to an <b>OH_Drawing_TextBlobBuilder</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_TextBlobBuilderDestroy(OH_Drawing_TextBlobBuilder*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
