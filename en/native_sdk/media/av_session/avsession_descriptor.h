/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_AVSESSION_DESCRIPTOR_H
#define OHOS_AVSESSION_DESCRIPTOR_H

/**
 * @addtogroup avsession
 * @{
 *
 * @brief Provides APIs for audio and video (AV) media control.
 *
 * The APIs can be used to create, manage, and control media sessions.
 *
 * @syscap SystemCapability.Multimedia.AVSession.Core
 * @since 9
 * @version 1.0
 */

/**
 * @file avsession_descriptor.h
 *
 * @brief Declares the session descriptor.
 *
 * @since 9
 * @version 1.0
 */

#include "parcel.h"
#include "element_name.h"

namespace OHOS::AVSession {
/**
 * @brief Describes the information about distributed devices.
 *
 * @since 9
 * @version 1.0
 */
struct OutputDeviceInfo {
    /** Whether the distributed devices are in the connected state. */
    bool isRemote_ {};
    /** IDs of the distributed devices. */
    std::vector<std::string> deviceIds_;
    /** Names of the distributed devices. */
    std::vector<std::string> deviceNames_;
};

/**
 * @brief Defines a session descriptor.
 *
 * @since 9
 * @version 1.0
 */
struct AVSessionDescriptor {
    /**
    * @brief Writes the session description into a {@link Parcel} object.
    *
    * @param out Indicates the pointer to the {@link Parcel} object.
    * @return Returns <b>true</b> if the operation is successful; returns <b>false</b> otherwise.
    * @see ReadFromParcel
    * @since 9
    * @version 1.0
    */
    bool WriteToParcel(Parcel& out) const;
    /**
    * @brief Reads the session description from a {@link Parcel} object.
    *
    * @param in Indicates the pointer to the {@link Parcel} object.
    * @return Returns <b>true</b> if the operation is successful; returns <b>false</b> otherwise.
    * @see WriteToParcel
    * @since 9
    * @version 1.0
    */
    bool ReadFromParcel(Parcel &in);
    /** Session ID. */
    std::string sessionId_;
    /** Session type. */
    int32_t sessionType_ {};
    /** Custom session name. */
    std::string sessionTag_;
    /** Information about the application to which the session belongs, including bundleName and abilityName. */
    AppExecFwk::ElementName elementName_;
    /** Process ID. */
    pid_t pid_ {};
    /** User ID */
    pid_t uid_ {};
    /** Whether the session is activated. */
    bool isActive_ {};
    /** Whether the session is the latest. */
    bool isTopSession_ {};
    /** Whether the application is a third-party application. */
    bool isThirdPartyApp_ {};
    /** Information about distributed devices. */
    OutputDeviceInfo outputDeviceInfo_;
};

/**
 * @brief Describes the basic session information.
 *
 * @since 9
 * @version 1.0
 */
struct AVSessionBasicInfo {
    /** Device name. */
    std::string deviceName_ {};
    /** Device ID. */
    std::string networkId_ {};
    /** Device vendor ID. */
    std::string vendorId_ {};
    /** Device type. */
    std::string deviceType_ {};
    /** System version. */
    std::string systemVersion_ {};
    /** Session version. */
    int32_t sessionVersion_ {};
    /** Remarks. */
    std::vector<int32_t> reserve_ {};
    /** Feature information. */
    std::vector<int32_t> feature_ {};
    /** Session metadata. */
    std::vector<int32_t> metaDataCap_ {};
    /** Playback states supported. */
    std::vector<int32_t> playBackStateCap_ {};
    /** System control commands. */
    std::vector<int32_t> controlCommandCap_ {};
    /** Extended capabilities. */
    std::vector<int32_t> extendCapability_ {};
    /** System time. */
    int32_t systemTime_ {};
    /** Extended information. */
    std::vector<int32_t> extend_ {};
};
}  // namespace OHOS::AVSession
#endif // OHOS_AVSESSION_DESCRIPTOR_H
