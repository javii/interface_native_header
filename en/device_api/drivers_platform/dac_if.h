/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 *
 * HDF is dual licensed: you can use it either under the terms of
 * the GPL, or the BSD license, at your option.
 * See the LICENSE file in the root of this repository for complete details.
 */
/**
 * @addtogroup DAC
 * @{
 *
 * @brief Provides digital-to-analog converter (DAC) APIs.
 *
 * This module allows the driver to perform operations on the DAC controller to access the devices on the DAC channels. The operations include opening or closing the DAC controller handle and writing data.
 *
 * @since 3.1
 */
/**
 * @file dac_if.h
 *
 * @brief Declares the standard DAC APIs.
 *
 * @since 3.1
 */	

#ifndef DAC_IF_H
#define DAC_IF_H

#include "platform_if.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

 /**
 * @brief Defines the structure of the messages transferred by a DAC device.
 */
struct DacIoMsg {
    /** DAC device number */
    uint32_t number;
    /** DAC device channel number */
    uint32_t channel;
};

 /**
 * @brief Opens the handle of a DAC controller.
 *
 * Call this API before a DAC device is accessed.
 *
 * @param number Indicates the DAC controller ID.
 *
 * @return Returns the pointer to the <b>DevHandle</b> pointer of the DAC controller if the operation is successful.
 * @return Returns <b>NULL</b> if the operation fails.
 *
 * @since 3.1
 */
DevHandle DacOpen(uint32_t number);

 /**
 * @brief Closes the handle of a DAC controller.
 *
 * Call this API to release the DAC controller so that unused memory resources can be released in time.
 *
 * @param handle Indicates the device handle to close.
 *
 * @since 3.1 
 */
void DacClose(DevHandle handle);

 /**
 * @brief Writes data to a DAC channel.
 *
 * @param handle Indicates the DAC device handle obtained by calling <b>DacOpen</b>.
 * @param channel Indicates the channel through which the DAC transmits messages.
 * @param val Indicates the value to write.
 *
 * @return Returns <b>0</b> if the operation is successful.
 * @return Returns a negative value if the operation fails.
 *
 * @since 3.1
 */
int32_t DacWrite(DevHandle handle, uint32_t channel, uint32_t val);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif /* DAC_IF_H */
